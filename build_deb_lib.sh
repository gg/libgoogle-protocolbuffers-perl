#!/bin/sh

epoch=0
version=`awk '/Version:/ {print $2}' debian/lib/control`
pkgname=`awk '/Package:/ {print $2}' debian/lib/control`
pkgdir=tmp/stage/$pkgname/$epoch/$version
mandir=$pkgdir/data/usr/share/man3

pmlnk.pm --copy --target $pkgdir/data/usr/share/perl5 google-protocolbuffers-perl/blib

cp google-protocolbuffers-perl/blib/lib/Google/ProtocolBuffers/CodecIV32.pm \
   google-protocolbuffers-perl/blib/lib/Google/ProtocolBuffers/CodecIV64.pm \
   $pkgdir/data/usr/share/perl5/Google/ProtocolBuffers

chmod 664 $pkgdir/data/usr/share/perl5/Google/ProtocolBuffers/CodecIV??.pm

mkdir -p $mandir
cp google-protocolbuffers-perl/blib/man3/Google::ProtocolBuffers.3pm $mandir

mkdir $pkgdir/control
cp debian/lib/control $pkgdir/control
echo "2.0" >$pkgdir/debian-binary

mkdeb.pl $pkgdir

